import styled from 'styled-components';

export const StyledLoginForm = styled.div`

.login-form {
    background-color: #e1e1e1;
    padding: 30px;
    border-radius: 10px;
    box-shadow: 0 3px 5px rgba(0,0,0,.25);
    display: flex;
    flex-direction: column;
}


.login-form__icon {
    color: rgba(0,0,0,.25);
}


`
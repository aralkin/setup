import React from 'react';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { StyledLoginForm } from './style'


const Login = ({ form }) => {

    const handleSubmit = e => {
        e.preventDefault();
   
        form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }     alert('Welcome!')
        });
    };


    const { getFieldDecorator } = form;


    return (
        <StyledLoginForm>
            <Form onSubmit={handleSubmit} className="login-form">
                <Form.Item>
                    {getFieldDecorator('username', {
                        rules: [{ required: true, message: 'Please input your username!' }],
                    })(
                        <Input
                            prefix={<Icon className="login-form__icon" type="user" />}
                            placeholder="Логин"
                        />
                        ,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                        <Input
                            prefix={<Icon className="login-form__icon" type="lock" />}
                            type="password"
                            placeholder="Пароль"
                        />,
                    )}
                </Form.Item>
                <Form.Item >
                    <a className="login-form__forgot" href="">
                        Забыли пароль?
                     </a>
                </Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form__submit">
                    Log in
                     </Button>
            </Form>
        </StyledLoginForm>

    )
};

const LoginForm = Form.create({ name: 'normal_login' })(Login);

export default LoginForm;

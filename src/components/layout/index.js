import React from 'react';
import { PageHeader } from 'antd'
import { Button } from 'antd';
import LoginForm from '../login-form'
import { StyledLayout } from './style'


const Layout = () => (
    <StyledLayout >
        <PageHeader
            title="Система заказов обедов. Панель администратора"
            className="header" />
        <section className="login">
            <LoginForm />
        </section>
        <footer className="footer">
            <Button type="primary" className="footer__button">Вход</Button>
        </footer>
    </StyledLayout>
)

export default Layout;

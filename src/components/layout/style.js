import styled from 'styled-components' 

export const StyledLayout = styled.div`
    box-sizing: border-box;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Hiragino Sans GB',
    'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji',
    'Segoe UI Emoji', 'Segoe UI Symbol';
    
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    height: 100vh;

.header {
    padding: 20px;
    width: 100%;
    border: 1px solid rgb(235, 237, 240);
    background-color: #e1e1e1;

}

.login {
    display: flex;
    align-items: center;
    justify-content: center;
}


.login__label {
    display: flex;
    align-items: center;
    justify-content: flex-end;
}

.login__input {
    margin-left: 20px;
}
.footer {
    width: 100%;
}

.footer__button{
    font-weight: 700;
    text-align: center;
    width: 100%;
}
`
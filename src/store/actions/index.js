const returnTypes = (string) => ({
  request: `${string.toUpperCase}_REQUEST`,
  success: `${string.toUpperCase}_REQUEST`,
  failed: `${string.toUpperCase}_FAILED`,
});


export const actions = returnTypes('YOUR_ACTION')
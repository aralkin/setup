import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import 'antd/dist/antd.css';
import Layout from './components/layout'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path='/*'>
           <Layout />
        </Route>
      </Switch>
    </BrowserRouter >
  );
}

export default App;
